<?php
require_once("./app/init.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link rel="shortcut icon" href="./Images/logo.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet" />
    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/home-style.css" />
    
    <title>Sonora's Quotation Manager</title>
</head>

<body>
    <div id = "head" class="container">
        <div id="nav" class="header container-fluid">
            <div class="row pt-3 justify-content-center">
                <div class="col-2 pt-2 pb-3 justify-content-center d-flex align-items-center">
                    <a class="nav-item" href="https://www.sonorasoftware.com/#services">
                        <img src="./Images/Sonora_logo.png" width="100%" alt="" srcset="" />
                    </a>
                </div>
                <div class="col-2 p-0 d-flex justify-content-center navbut align-items-center" id="navbut-wrapper-1"
                    data-nav-id="1">
                    <div 
                        class="w-100 h-100 d-flex align-items-center justify-content-center nav-item text-decoration-none">
                        Home
                    </div>
                </div>
                <div class="col-2 p-0 d-flex justify-content-center navbut align-items-center" id="navbut-wrapper-2"
                    data-nav-id="2">
                    <div 
                        class="w-100 h-100 d-flex nav-item align-items-center justify-content-center text-decoration-none">
                        Quotation System
                    </div>
                </div>
                <div class="col-2 p-0 d-flex justify-content-center navbut align-items-center" id="navbut-wrapper-3"
                    data-nav-id="3">
                    <div 
                        class="w-100 h-100 d-flex nav-item align-items-center justify-content-center text-decoration-none">
                        About Us
                    </div>
                </div>
                <div class="col-2 p-0 d-flex justify-content-center navbut align-items-center" id="navbut-wrapper-4"
                    data-nav-id="4">
                    <div
                        class="w-100 h-100 d-flex nav-item align-items-center justify-content-center text-decoration-none">
                        Contact Us
                    </div>
                </div>
                <div class="col-1 pb-3 d-flex justify-content-center navbut align-items-center">
                    <a type="button" id="loginbut" class="btn btn-primary rounded-pill button" href="login.php">Login</a>
                </div>
                <div class="col-1 pb-3 d-flex justify-content-center navbut align-items-center">
                <a type="button" id="loginbut" class="btn btn-primary rounded-pill button" href="signup.php">Sign Up</a>
                </div>
            </div>
        </div>
        <div class="container w-100 hero-div">
            <div class="row mb-5 mt-5">
                <div class="col-6 p-5 d-flex justify-content-center text-center align-items-center">
                    <h1 class="hero-text">IT solutions that spell quality</h1>
                </div>
                <div class="col-6 p-5 d-flex justify-content-center">
                    <img src="./Images/tab.png" class="hero-img visible-md visible-lg" />
                </div>
            </div>
        </div>
    </div>

    <!-- Quotation System Navigator -->
    <div id="QuoSysNav" class="container-fluid pt-5 pb-5">
        <div class="container">
            <div class="col mb-5 d-flex flex-column align-items-center justify-content-center">
                <h2 class="mb-5 mt-5"><b>Quotation Management System</b></h2>
                <p class="aboutdiv">
                    Want a better experience with managing your product's Quotations ? 
                </p>
                <div class="p-4">
                    <a href="login.php">
                        <button type="button" class="btn btn-primary button button-ext">Check it out</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </pb-5>
    <!-- About Us -->
    <div id="aboutus" class="container-fluid pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col mb-5 d-flex flex-column align-items-center justify-content-center">
                    <h2 class="mb-5 mt-5"><b>ABOUT US</b></h2>
                    <p class="aboutdiv">
                        Sonora Software Pvt Ltd located out of Pune, India has a diverse portfolio of being a software products as well as services provider company. We have several clients from diverse domains ranging from construction, education, legal, healthcare to airline industry. With a keen focus on technology, we solve complex problems for our clients using next generation technologies such as Mobility, E-commerce, Analytics Services & Social Collaboration to address their developing business needs. Strong technical hands on and greater domain knowledge have proved Sonora as a key technology partner in the success stories of our customers. We believe in building long term relationships with our customers while defining the technology roadmap and delivering the value along with high quality deliverables!
                    </p>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-6 d-flex flex-column align-items-center justify-content-center">
                    <h4 class="text-black mb-4">VISION</h4>
                    <p class="aboutdiv">
                        Our vision is to build software products and solutions that address complex business problems. Our philosophy is to build robust software solution having a front user interface that is extremely easy to navigate and use
                    </p>
                </div>
                <div class="col-6 d-flex flex-column align-items-center justify-content-center">
                    <h4 class="text-black mb-4">MISSION</h4>
                    <p class="aboutdiv">
                        At Sonora, our mission is to create software product and solutions for our customers that will help them manage their work efficiently and effectively. We achieve this by understanding customer needs and adopting current software technologies to create robust software solutions
                    </p>    
                </div>
            </div>
        </div>
    </div>
    <!-- Contact Us -->
    <div id="contactus" class="container-fluid mt-5 mb-5 p-0">
            <div class="row d-flex">
                <div id="contact-div" class="col-6">
                    <h2 class="mb-4"><b>Contact Us</b></h2>
                    <h5 class="mb-3"><b>DEVELOPMENT CENTER</b></h5>
                    <h6 class="text-center">A/502 Teerth Technospace,<br>Behind BU Bhandari Mercedes Showroom,<br>Mumbai-Bangalore Highway, Baner, Pune 411045</h6>
                    <h5 class="mt-4 mb-3"><b>CONTACT</b></h5>
                    <h6 class="text-center">+91 9503600151<br>contact@sonorasoftware.com</h6>
                </div>
                    <iframe class="col-6" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.227541745754!2d73.76309347519275!3d18.56377758253818!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2beb474f086eb%3A0xa13ff8be178ff4dd!2sSonora%20Software%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1706857513199!5m2!1sen!2sin" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
    </div>
    <script src="js/main.js"></script>
</body>

</html>