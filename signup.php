<?php
require_once('./app/init.php');
$error = false;
if(isset($_POST['signup'])) {
    $rules = [
        'firstname' => [
            'required' => true,
            'minLength' => 3,
            'maxLength' => 255
        ],
        'lastname' => [
            'required' => true,
            'minLength' => 3,
            'maxLength' => 255
        ],
        'email' => [
            'required' => true,
            'minLength' => 5,
            'maxLength' => 255,
            'email' => true,
            'unique' => 'users.email'
        ],
        'password' => [
            'required' => true,
            'minLength' => 8,
            'maxLength' => 255
        ],
        'confirm-password' => [
            'required' => true,
            'minLength' => 8,
            'maxLength' => 255
        ]
    ];

    $validator->check($_POST, $rules);

    if(!$validator->fails()) {
        $data['firstname'] = $_POST['firstname'];
        $data['lastname'] = $_POST['lastname'];
        $data['email'] = $_POST['email'];
        $data['role'] = 1;
        $password = $_POST['password'];
        $confirmPassword = $_POST['confirm-password'];
            
        if($password === $confirmPassword) {
            $error = false;
            $data['password'] = $_POST['password'];
            if(User::create($connection, $data)) {
                Auth::signin($connection, $data['email'], $data['password']);
                redirect("quotationSys.php");
            } else {
                $error = true;
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    <script src="js/bootstrap.bundle.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/home-style.css">
    <link rel="stylesheet" href="css/signup-style.css">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
    <div class="container">
        <div class="card">
            <h3>Sign-Up</h3>
            <!-- <p id = "signup-caption">Be a part of our organization!</p> -->
            <form action="<?= $_SERVER['PHP_SELF'];?>" method="POST">
                <div class="controls">
                    <div class="fullname">
                        <div class="d-flex flex-column me-2">
                            <label for="firstname" class="form-label mb-0">Firstname</label>
                            <input type="text" name="firstname" id="firstname" class = "name inputs form-control w-100 mt-1" placeholder="Enter first name" value="<?= old($_POST, 'firstname'); ?>">
                            <span class="error-message" id="firstnameError">
                                <?= $validator->errors()->has('firstname') ? $validator->errors()->first('firstname') : '' ;?>
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <label for="lastname" class="form-label mb-0">Lastname</label>
                            <input type="text" name="lastname" id="lastname" class = "name inputs form-control w-100 mt-1" placeholder="Enter last name" value="<?= old($_POST, 'lastname'); ?>">
                            <span class="error-message" id="lastnameError">
                                <?= $validator->errors()->has('lastname') ? $validator->errors()->first('lastname') : '' ;?>
                            </span>
                        </div>
                    </div>
                    <label for="mail" class="form-label mb-0">Email address</label>
                    <input type="text" name="email" id="mail" placeholder="Enter email" class="inputs form-control mt-1" value="<?= old($_POST, 'email'); ?>">
                    <span class="error-message" id="emailError">
                        <?= $validator->errors()->has('email') ? $validator->errors()->first('email') : '' ;?>
                    </span>
                    <label for="pwd" class="form-label mb-0">Password</label>
                    <div class="input-group mt-1">
                        <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter Password">
                        <button class="btn btn-outline-secondary" type="button" id="showhidepass"><i class="bi bi-eye"></i></button>
                    </div>
                    <span class="error-message" id="passwordError">
                        <?= $validator->errors()->has('password') ? $validator->errors()->first('password') : '' ;?>
                    </span>
                    <label for="con-pwd" class="form-label mb-0 mt-3">Confirm Password</label>
                    <div class="input-group mt-2">
                        <input type="password" name="confirm-password" class="form-control" id="con-pwd" placeholder="Enter Password">
                        <button class="btn btn-outline-secondary" type="button" id="con-showhidepass"><i class="bi bi-eye"></i></button>
                    </div>
                    <span class="error-message" id="confirmPasswordError">
                        <?= $validator->errors()->has('confirm-password') ? $validator->errors()->first('confirmpassword') : '' ;?>
                        <?= $error ? "Confirm Password is not same as New Password" : '' ;?>
                    </span>
                    <div class="btn-holder mt-2">
                        <input type="submit" name="signup" class="btn btn-primary rounded-pill button w-50 ps-4 pe-4" id="submitSignUp">
                    </div>
                </div>
            </form>
            <div class="d-flex w-90 mt-1 justify-content-center align-items-center" style="font-size: small;">
                Already have an account?&nbsp;<a href="login.php" style="font-size: small;">Log In</a>
            </div>
        </div>
    </div>
    <script src="js/signup.js"></script>
</body>
</html>