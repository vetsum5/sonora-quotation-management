var pwdctr = 0;
$("document").ready(function(){
    $('#showhidepass').click(function(){
        if(pwdctr == 0){
            document.getElementById("pwd").setAttribute("type", "text");
            document.getElementById("showhidepass").innerHTML = '<i class="bi bi-eye-slash"></i>';
            pwdctr = 1;
        } else {
            document.getElementById("pwd").setAttribute("type", "password");
            document.getElementById("showhidepass").innerHTML = '<i class="bi bi-eye"></i>';
            pwdctr = 0;
        }
    });
});