let nameOfProject = "";
let numberOfUserInputs = 0;
let numberOfUserOutputs = 3;
let userInquires = 5;
let numberOfFiles = 5;
let externalInterfaces = 2;
let projectLevel = "";
let noOfEngineers = 0;
let noOfTesters = 0;
let functionPoint = 0;
let countTotal = 0;
let effort = 0;
let time = 0;
let cost = 0;


const productivityFactor = 2.2;
const fixedChargesPerMonth = 1500;
const salaryEngineerPerMonth = 15600;
const salaryTesterPerMonth = 7500;

simpleMulipliers = [3,4,3,7,5];
averageMultipliers = [4,5,4,10,7];
complexMultipliers = [6,7,6,15,10];

paramValues = [0,3,5,5,2];
valuesParamters = [[3,2,2,1,1], [2,2,1,1,1], [2,3,1,1,0], [0,1,3,3,1], [0,1,1,1,1], [4,2,1,1,1], [0,0,1,0,1], [0,0,1,0,1]];
questions = [
    ["What is the name of your project","str"],
    ["Does your project have a register page?","y/n"],
    ["Does your project have a login page?", "y/n"],
    ["Does your project have dashboard page?", "y/n"],
    ["Does your project consist of database connectivity?", "y/n"],
    ["Will you web host your project?", "y/n"],
    ["Does your project consist of contact-us page?", "y/n"],
    ["Will your project store cookies?" , "y/n"],
    ["Will your project manage session?", "y/n"],
    ["How many database servers would your project require?", "num"],
    ["How many external links would you add to your project website?", "num"],
    ["Enter number of miscellaneoues files you may provide (readme, documentation, etc)", "num"], 
    ["Enter number of engineers you wish to employee","num"],
    ["Enter number of testers you wish to employee","num"],
    ["Rate the expected backup recovery of system from 1 to 5", "rating"],
    ["Rate the data communication speed expected from 1 to 5", "rating"],
    ["Rating for distributed processing functions from 1 to 5", "rating"],
    ["Performance level expected from 1 to 5", "rating"],
    ["Rate the heaviness of running environment from 1 to 5", "rating"],
    ["Rate the speed of online entry of database system from 1 to 5", "rating"],
    ["Rate speed for updation of logical files from 1 to 5","rating"],
    ["Rate complexity of input , output files from 1 to 5","rating"],
    ["Rate complexity of internal processing from 1 to 5", "rating"],
    ["Rate reusability expected from your code from 1 to 5", "rating"],
    ["Rate the convinience level of installation expected from 1 to 5", "rating"],
    ["Rate multiprocessing power expected from the system from 1 to 5","rating"],
    ["Rate 'ability to evolve' capability expected from 1 to 5", "rating"]
];
end = false;
answerBits = [];
let currentPointer = 0;
questionListDiv = document.getElementById("qaList");
function registerEventListeners() {
    document.addEventListener("DOMContentLoaded", askQuestion)
    document.getElementById("inputForm").addEventListener("submit", getAnswer);
}

registerEventListeners();


function isPositive(pointer, answer) {
    satisfier = questions[pointer][1];
    if(satisfier == 'y/n' && answer  == 1) {
        return true;
    }
    return false;
}

function convertAnswerBits() {
    nameOfProject = answerBits[0];
    for (let i = 1; i < 9; i++) {
        currentAnswer = answerBits[i];
        if(isPositive(i, currentAnswer)) {
            for (let j = 0; j < 5; j++) {
                paramValues[j] += valuesParamters[i-1][j]
                
            }
        }
    }

    // Calculating other expenses

    for (let i = 9; i < 14; i++) {
        
        if(i == 9) {
            paramValues[4] += answerBits[i];
        }
        else if( i== 10){
            paramValues[4] += answerBits[i];
        }
        else if(i==11) {
            paramValues[3] += answerBits[i];
        }
        else if(i == 12){
            noOfEngineers = answerBits[i];
        }
        else if(i==13) {
            noOfTesters = answerBits[i];
        }
        
    }
    
    // Calculating simplex, average , complex
    total = 0;
    for (let i = 14; i < 27; i++) {
        answer = parseInt(answerBits[i]);
        console.log(answer);
        total += answer;    
        
    }
    if(total >=0 && total<=23) {
        projectLevel = "Simple";
    }
    else if(total >= 24 && total <= 52) {
        projectLevel = "Average";
    }
    else if(total >= 53 && total<=70) {
        projectLevel = "Complex";
    }

    calculateFp(paramValues, projectLevel, total);

}

function calculateFp(arrayParamValues, complexity, complexityNumber) {
    if(complexity == "Simple") {
        useArray = simpleMulipliers;
    }
    else if(complexity == "Average") {
        useArray =averageMultipliers;
    }
    else if(complexity == "Complex") {
        useArray = complexMultipliers;
    }

    finalParamValues = [];
    for (let i = 0; i < 5; i++) {
        finalParamValues[i] = arrayParamValues[i] * useArray[i];
        
    }

    
    for (let i = 0; i < 5; i++) {
        countTotal+=finalParamValues[i];
    }

    fp = 0;
    fp = countTotal * [0.65 + (0.01 * complexityNumber)];

    functionPoint = Math.ceil(fp);
    effort = Math.ceil(functionPoint / productivityFactor);
    if(noOfEngineers == 0) {
        noOfEngineers = 1;
    }
    if(noOfTesters == 0) {
        noOfTesters = 1;
    }
    time = effort/(noOfEngineers + noOfTesters);
    cost = (noOfEngineers * salaryEngineerPerMonth + noOfTesters * salaryTesterPerMonth  + fixedChargesPerMonth) * time;
    // console.log("Function point = " + functionPoint);
    // console.log("Effort = " + effort + " person month");
    // console.log("Time = " + time + " months");
    // console.log("Cost = " + cost + " rupees");
    // console.log("Use Array = " + useArray);
    // console.log("Final Param Values = " + finalParamValues);
    // console.log("Array Param Values = " + arrayParamValues);
    // console.log("Complexity number = " + complexityNumber);
    numberOfUserInputs = finalParamValues[0];
    numberOfUserOutputs = finalParamValues[1];
    userInquires = finalParamValues[2];
    numberOfFiles = finalParamValues[3];
    externalInterfaces = finalParamValues[4];
    sendDataToPhp();

}

function sendDataToPhp() {
    resultForm = document.getElementById("resultForm");

    resultForm.innerHTML = 
    `
    <input type = "hidden" name = "nameOfProject" value = "${nameOfProject}">
    <input type = "hidden" name = "noOfInputs" value = "${numberOfUserOutputs}">
    <input type = "hidden" name = "noOfOutputs" value = "${numberOfUserOutputs}">
    <input type = "hidden" name = "noOfFiles" value = "${numberOfFiles}">
    <input type = "hidden" name = "noOfExternal" value = "${externalInterfaces}">
    <input type = "hidden" name = "noOfEng" value = "${noOfEngineers}">
    <input type = "hidden" name = "noOfTesters" value = "${noOfTesters}">
    <input type = "hidden" name = "total" value = "${countTotal}">
    <input type = "hidden" name = "functionPoint" value = "${functionPoint}">
    <input type = "hidden" name = "projectType" value = "${projectLevel}">
    <input type = "hidden" name = "effort" value = "${effort} person months">
    <input type = "hidden" name = "time" value = "${Math.ceil(time)} months">
    <input type = "hidden" name = "cost" value = "${Math.ceil(cost)} rupees">
    `;

    resultForm.submit();
}
function askQuestion() {
    
    currentQuestion = questions[currentPointer][0];
    insertNewItem(currentQuestion, "question");

}

function insertNewItem(itemText, type, error = false) {
    newItem = document.createElement("div");
    if(type == 'question') {
        newItem.innerHTML = `
        <div href="#" class="list-group-item list-group-item-action chat-list-response" aria-current="true">
            <div class="d-flex w-100 gap-2">
                <img src="./Images/logo.png" width="3%" height="3%" alt="">
                <h5 class="mb-1">Quotation Master</h5>
            </div>
            <p class="mb-1 ms-5 type-animation">${itemText}</p>
        </div>
        `
    }
    else if(type == 'answer') {
        if(!error) {
            newItem.innerHTML = `<div href="#" class="list-group-item list-group-item-action chat-list-response">
        <div class="d-flex w-100 gap-2">
            <img src="./Images/Users/empty_user.png" width="3%" height="3%" alt="">
            <h5 class="mb-1">Username</h5>
        </div>
        <p class="mb-1 ms-5">${itemText}</p>
    </div>`; 
        } else {
            newItem.innerHTML = `<div href="#" class="bg-danger list-group-item list-group-item-action chat-list-response">
        <div class="d-flex w-100 gap-2">
            <img src="./Images/Users/empty_user.png" width="3%" height="3%" alt="">
            <h5 class="mb-1">Username</h5>
        </div>
        <p class="mb-1 ms-5">${itemText}</p>
    </div>`; 
        }
    }
    questionListDiv.appendChild(newItem);
    questionListDiv.scrollTop = questionListDiv.scrollHeight;
}

function validateAnswer(currentPointer , answer) {
    satisfier = questions[currentPointer][1];
    if(satisfier == 'y/n') {
        if(answer.toString().toLowerCase().charAt(0) == 'y' || answer.toString().toLowerCase().charAt(0) == 'n'){
            return true;
        }
        return false;
    }
    else if(satisfier == 'num') {
        if(!isNaN(answer) && parseInt(answer)>=0) {
            return true;
        }
        return false;
    }
    else if(satisfier == 'str') {
        return true;
    }

    else if(satisfier == 'rating') {
        // console.log("Here");
        if(!isNaN(answer)) {
            if(parseInt(answer)>=1&&parseInt(answer)<=5){
                return true;
            }
            // console.log("Here");
            
        }
        return false;
    }
    return false;
}

function extractAnswerAndInsertIntoArray(currentPointer, answerString) {
    let satisfier = questions[currentPointer][1];
    if(satisfier == 'y/n') {
        if(answerString.toString().toLowerCase().charAt(0) == 'y') {
            answerBits.push(1);
        }
        else{
            answerBits.push(0);
        }
    }
    else if(satisfier == 'num') {
        answerBits.push(Number(answerString));
    }
    else if(satisfier == 'rating') {
        answerBits.push(Number(answerString));
    }
    else if(satisfier == 'str') {
        answerBits.push(answerString);
    } 
}

function getAnswer(e) {
    e.preventDefault();
    
        let answerString = document.getElementById("answerTextField").value;
        
        if(answerString.toString().trim()!="") {
            if(validateAnswer(currentPointer, answerString)) {
                insertNewItem(answerString, "answer");
                extractAnswerAndInsertIntoArray(currentPointer, answerString);
                currentPointer++;
            }
            else{
                insertNewItem(answerString, "answer", true);
            }
            document.getElementById("answerTextField").value = "";
            
        
            if(currentPointer < questions.length) {
                askQuestion();
            }
            else{
                
                convertAnswerBits();
            }
        }
}
$(function(){
    $('.remove-quot').click(function(e){
        $('.lg-style').click(function(evt){
            e.preventDefault();
        });
        $('#delete-id-input').val($(this)[0].dataset.id);
    });
    $('.placeorder').click(function(e){
        $('.lg-style').click(function(evt){
            e.preventDefault();
        });
    });
});