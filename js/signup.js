var pwdctr = 0;
$("document").ready(function(){
    $('#showhidepass').click(function(){
        if(pwdctr == 0){
            document.getElementById("pwd").setAttribute("type", "text");
            document.getElementById("showhidepass").innerHTML = '<i class="bi bi-eye-slash"></i>';
            pwdctr = 1;
        } else {
            document.getElementById("pwd").setAttribute("type", "password");
            document.getElementById("showhidepass").innerHTML = '<i class="bi bi-eye"></i>';
            pwdctr = 0;
        }
    });
});
var conpwdctr = 0;  
$("document").ready(function(){
    $('#con-showhidepass').click(function(){
        if(conpwdctr == 0){
            document.getElementById("con-pwd").setAttribute("type", "text");
            document.getElementById("con-showhidepass").innerHTML = '<i class="bi bi-eye-slash"></i>';
            conpwdctr = 1;
        } else {
            document.getElementById("con-pwd").setAttribute("type", "password");
            document.getElementById("con-showhidepass").innerHTML = '<i class="bi bi-eye"></i>';
            conpwdctr = 0;
        }
    });
});