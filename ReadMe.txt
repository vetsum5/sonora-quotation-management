composer:-
composer init
composer require phpmailer/phpmailer

sql file:-
sql/users.sql
sql/tokens.sql

config.ini:-
[app]
app_name="Quotation Management System"
app_url="http://localhost:8888"
app_debug="true"

[database]
driver_name=
host=
username=
password=
database="qms"
port=3306

[mail]
from_mail_address="sonoraquotationmanager@gmail.com"