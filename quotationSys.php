<?php
  require_once("./app/init.php");
  // if("15 Mar 2024" == date("d M Y")){
  //   dd("Today");
  // } else {
  //   dd(date("d M Y"));
  // }
  if(isset($_SESSION['logged_in_user']) && !empty($_SESSION['logged_in_user'])){
    $quots = Quotation::findByEmail($connection,$_SESSION['logged_in_user']['email']);
    // if($quots){
    //   dd($quots);
    // }
  } else{
    redirect("login.php");
  }

  if(isset($_POST['loadquotid']) && !empty($_POST['loadquotid'])){
    $res = Quotation::find($connection,$_POST['loadquotid']);
    if($res){
      $_SESSION['quotation_name'] = $res['quotation_name'];
      $_SESSION['email'] = $res['email'];
      $_SESSION['date'] = $res['date'];
      $_SESSION['num_of_inputs'] = $res['num_of_inputs'];
      $_SESSION['num_of_outputs'] = $res['num_of_outputs'];
      $_SESSION['num_of_files'] = $res['num_of_files'];
      $_SESSION['num_of_external'] = $res['num_of_external'];
      $_SESSION['num_of_engineers'] = $res['num_of_engineers'];
      $_SESSION['num_of_testers'] = $res['num_of_testers'];
      $_SESSION['total'] =  $res['total'];
      $_SESSION['fp'] = $res['fp'];
      $_SESSION['project_level'] = $res['project_level'];
      $_SESSION['effort'] = $res['effort'];
      $_SESSION['time'] = $res['time'];
      $_SESSION['cost'] = $res['cost'];
      redirect('./MakePdf.php');
    } else {
      echo "Error fetching data";
    }
  }

  if(isset($_POST['delete-id']) && !empty($_POST['delete-id'])){
    $delres = Quotation::deleteQuotation($connection,$_POST['delete-id']);
    if($delres){
      redirect("quotationSys.php");
    }
  }

  if(isset($_POST['logout'])){
    Auth::logout($connection);
    redirect('index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/bootstrap.bundle.js" ></script>
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/quotation.css">
    <title>Sonora | Quotation System</title>
	  
</head>
<body class="container-fluid d-flex align-items-center p-0">
    <div class="col-3 flex-column d-flex sidenav gap-3">
        <div class="newchat align-items-center w-1 justify-content-between d-flex col-12">
            <div class="d-flex align-items-center flex-row gap-2">
              <img src="./Images/logo.png" class="quot-head-main-img" alt="" srcset="">
              <span class="quot-head-main">Welcome, <?= substr($_SESSION['logged_in_user']['email'],0,strpos($_SESSION['logged_in_user']['email'],'@')) ?></span>
              
            </div>
            <form action="quotationSys.php" method="post">
              <button class="btn btn-warning" type="submit" name="logout">
                Logout
              </button>
            </form>
        </div>
          <!-- Delete Toast -->
          <div class="toast-container position-fixed bottom-0 end-0 p-3">
            <div id="deleteToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
              <div class="toast-header">
                <img src="./Images/logo.png" width="5%" height="5%" alt="">
                <strong class="me-auto">&nbsp;Quotation Master</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
              </div>
              <div class="toast-body bg-danger-subtle">
                Quotation deleted successfully!
              </div>
            </div>
          </div>
          <!-- Create Toast -->
          <div class="toast-container position-fixed bottom-0 end-0 p-3">
            <div id="createToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
              <div class="toast-header">
                <img src="./Images/logo.png" width="5%" height="5%" alt="">
                <strong class="me-auto">&nbsp;Quotation Master</strong>
                <small>now</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
              </div>
              <div class="toast-body bg-success-subtle">
                Quotation created successfully!
              </div>
            </div>
          </div>
        <!-- Reset Modal -->
        <div class="modal fade" id="resetModal" tabindex="-1" aria-labelledby="resetModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h1 class="modal-title fs-5" id="resetModalLabel">Reset Quotation</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                All progress will be lost, are you sure?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-warning"  id="resetBtn" data-bs-dismiss="modal" onclick="location.reload()">Reset</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Add Modal -->
        <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h1 class="modal-title fs-5" id="addModalLabel">Create New Quotation</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <form action="quotationSys.php" method="post">
                <div class="modal-body">
                  <label for="addquotnameinp" class="form-label">Enter name of the project</label>
                  <input type="hidden" name="quotemail" value="<?= $_SESSION['logged_in_user']['email'] ?>">
                  <input type="text" name="addquotname" class="form-control" id="emailinp" name="email" placeholder="Name of Project">
                  <input type="hidden" name="adddate" value="<?= date("Y-m-d") ?>">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-success" id="addnewquot">Continue</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Delete Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h1 class="modal-title fs-5" id="deleteModalLabel">Delete Quotation</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                Permanently remove this quotation, are you sure?
              </div>
              <div class="modal-footer">
                <form action="quotationSys.php" method="post">
                  <input type="hidden" name="delete-id" id="delete-id-input">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-danger">Delete</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- placeorder Modal -->
        <div class="modal fade" id="placeorderModal" tabindex="-1" aria-labelledby="placeorderModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h1 class="modal-title fs-5" id="placeorderModalLabel">Order Quotation</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                Your order has been placed successfully!
              </div>
              <div class="modal-footer">
                <form action="quotationSys.php" method="post">
                  <input type="hidden" name="delete-id" id="delete-id-input">
                  <button type="button" class="btn btn-success" data-bs-dismiss="modal">OK</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="current flex-column gap-2 w-1 d-flex col-12">
            <span class="quot-head-main">Current Quotation</span>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action lg-style" aria-current="true">
                  <div class="d-flex w-100 justify-content-between">
                    <span class="mb-1 quot-head">New Quotation</span>
                    <span style="font-size: x-small;">Just now</span>
                  </div>
                  <div class="d-flex justify-content-between lg-style-inner-small">
                    <small class="mb-1 quot-topic">Quotation for example product</small>
                    <i class="bi bi-trash remove-quot"></i>
                  </div>
                </a>
            </div>
        </div> -->
        <div class="previous flex-column gap-2 d-flex col-12">
            <span class="quot-head-main">Your Quotations</span>
            <div class="list-group" id="quotList">
                <?php
                  if($quots):
                ?>
                  <?php
                    foreach($quots as $quot):
                  ?>
                  <form action="quotationSys.php" method="post">
                    <input type="hidden" name="loadquotid" value="<?= $quot["id"] ?>">
                    <button type="submit" href="#" class="list-group-item list-group-item-action lg-style" aria-current="true">
                      <div class="d-flex w-100 justify-content-between">
                        <span class="mb-1 quot-head"><?= $quot["quotation_name"] ?></span>
                        <span style="font-size: x-small;"><?= $quot["date"] == date("Y-m-d") ? "Today" : $quot["date"] ?></span>
                      </div>
                      <div class="d-flex justify-content-between lg-style-inner-small">
                        <small class="mb-1 quot-topic">Quotation for <?= $quot["quotation_name"] ?></small>
                        <i class="bi bi-trash remove-quot" data-id="<?= $quot["id"] ?>" data-bs-toggle="modal" data-bs-target="#deleteModal"></i>
                      </div>
                      <form action="quotationSys.php" method="post">
                        <input type="button" class="btn btn-success w-100 placeorder" id="placeorder" value="Place Order" data-bs-toggle="modal" data-bs-target="#placeorderModal">
                      </form>
                    </button>
                  </form>
                  <?php
                    endforeach;
                  ?>
                <?php
                  else: 
                ?> 
                  <div class="w-100 container d-flex justify-content-center align-items-center">
                    <span class="quot-head" style="color: grey">No Quotations found</span>
                  </div>
                <?php
                  endif;
                ?>
            </div>
        </div>
    </div>
    <div class="col-9 d-flex gap-2 flex-column-reverse justify-content-center align-items-center mainchat">
        
		<form action="" class = "input-group" id = "inputForm">
			<input class="form-control form-control-lg main-inp" id = "answerTextField" type="text" placeholder="Type here..." aria-label=".form-control-lg example">
			<button class="btn send-inp" type="submit"><i class="bi bi-send"></i></button>
		</form>
        
        <div>
            
        </div>
        <?php
          if(isset($currentqout) && !empty($currentqout) && $currentqout[0]['q1_response'] != NULL):
        ?>
              <div class="list-group w-100 chat-response-section">
                <?php
                  foreach($currentqout[0] as $ans):
                ?>
                      <div href="#" class="list-group-item list-group-item-action chat-list-response">
                          <div class="d-flex w-100 gap-2">
                              <img src="./Images/Users/empty_user.png" width="3%" height="3%" alt="">
                              <h5 class="mb-1">Username</h5>
                          </div>
                          <p class="mb-1 ms-5"><?= $ans ?></p>
                      </div>
                <?php
                  endforeach;
                ?>
              </div>
        <?php
          else:
        ?>
            <div class="list-group w-100 chat-response-section" id = "qaList">
            </div>
        <?php
          endif;
        ?>
        <?php
          if(isset($currentqout) && !empty($currentqout) && $currentqout[0]['q1_response'] != NULL):
        ?>
            <div class="d-flex flex-row-reverse w-100">
              <div title="Cannot reset saved quotations">
                <button class="btn btn-reset text-secondary" type="button"><i class="bi bi-arrow-clockwise"></i></button>
              </div>
            </div>
          <?php
            else:
          ?>
            <div class="d-flex flex-row-reverse w-100">
              <div class="btn-reset-wrapper"  data-bs-toggle="modal" data-bs-target="#resetModal">
                <button class="btn btn-reset" type="button"><i class="bi bi-arrow-clockwise"></i></button>
              </div>
            </div>
          <?php
            endif;
          ?>  
    	</div>

	</div>
	<div class="div empty-form">
		<form action="generate-bill.php" method="POST" id = "resultForm"></form>
	</div>
  <?php
          if(isset($delres) && $delres == true):
        ?>
          <script>
            var toastBootstrap = bootstrap.Toast.getOrCreateInstance(document.getElementById('createToast'));
            toastBootstrap.show();
          </script>
        <?php
          endif;
        ?>
        <?php
          if(isset($createRes) && $createRes == true):
        ?>
          <script>
            var toastBootstrap = bootstrap.Toast.getOrCreateInstance(document.getElementById('deleteToast'));
            toastBootstrap.show();
          </script>
        <?php
          endif;
        ?>
        <script src="js/quotation.js"></script>
</body>
</html>