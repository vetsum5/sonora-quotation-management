<?php
require_once("./app/init.php");

// Check if you are landing without token
if(!isset($_GET['t']) && !isset($_POST['t'])) {
    redirect("index.php");
}

if(isset($_GET['t'])) {
    $t = $_GET['t'];
} else {
    $t = $_POST['t'];
}

$tokenData = Token::isValidForgotPasswordToken($connection, $t);
if(!$tokenData) {
    ed('Your link has been expired! Please <a href="forgot-password.php">click here</a> to generate a new one!');
}
$error = false;
if(isset($_POST['reset'])) {
    $rules = [
        'newpassword' => [
            'required' => true,
            'minLength' => 8,
            'maxLength' => 255
        ],
        'confirmpassword' => [
            'required' => true,
            'minLength' => 8,
            'maxLength' => 255
        ]
    ];

    $validator->check($_POST, $rules);

    if(!$validator->fails()) {
        $newPassword = $_POST['newpassword'];
        $confirmPassword = $_POST['confirmpassword'];

        if($newPassword === $confirmPassword) {
            $error = false;
            $data['password'] = $newPassword;
            $result = User::updatePassword($connection, $tokenData['user_id'], $data);
            if($result) {
                Token::deleteForgotPasswordToken($connection, $tokenData['user_id']);
                $user = User::find($connection, $tokenData['user_id']);
                Auth::setLoggedInUser($user);
                redirect("quotationSys.php");
            } else {
                dd("Some issue in updating password!");
            }
        } else {
            $error = true;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link rel="shortcut icon" href="./Images/logo.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/home-style.css" />
    <link rel="stylesheet" href="css/loginstyle.css">
    <link rel="stylesheet" href="css/reset-password.css" />
    
    <title>Reset Password</title>
</head>
<body>
    <div class="container d-flex col-4 align-items-center justify-content-center card pt-5 pb-5 ">
        <form class="card-body w-100 d-flex align-items-center justify-content-center flex-column" action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
            <h3>Reset Password</h3>
            <div class="mb-3 mt-2 w-75">
                <label for="pwd" class="form-label">New Password</label>
                <div class="input-group">
                    <input type="password" name="newpassword" class="form-control" id="pwd" placeholder="Enter Password">
                    <button class="btn btn-outline-secondary" type="button" id="showhidepass"><i class="bi bi-eye"></i></button>
                </div>
            </div>
            <div class="mb-3 w-75">
                <label for="con-pwd" class="form-label">Confirm Password</label>
                <div class="input-group">
                    <input type="password" name="confirmpassword" class="form-control" id="con-pwd" placeholder="Re-enter Password">
                    <button class="btn btn-outline-secondary" type="button" id="con-showhidepass"><i class="bi bi-eye"></i></button>
                </div>
            </div>
            <input type="hidden" name="t" value="<?=$t;?>">
          <div class="d-flex w-75 mt-3 justify-content-center align-items-center">
            <button type="submit" name="reset" id="loginbut" class="btn btn-primary rounded-pill button w-50 ps-4 pe-4">Reset</button>
          </div>
        </form>
    </div>
    <script src="js/main.js"></script>
    <script src="js/reset-password.js"></script>
</body>
</html>