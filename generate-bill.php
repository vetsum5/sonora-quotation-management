<?php

require_once("./app/init.php");
$insRes = false;
$email = $_SESSION['logged_in_user']['email'];
$data = [];
if(isset($_POST)) {
    // var_dump($_POST);
    $date = date("Y-m-d");
    $queryBuilder = new QueryBuilder($connection);
    
    $data['quotation_name'] = $_POST['nameOfProject'];
    $data['email'] = $_SESSION['logged_in_user']['email'];
    $data['date'] = $date;
    $data['num_of_inputs'] = $_POST['noOfInputs'];
    $data['num_of_outputs'] = $_POST['noOfOutputs'];
    $data['num_of_files'] = $_POST['noOfFiles'];
    $data['num_of_external'] = $_POST['noOfExternal'];
    $data['num_of_engineers'] = $_POST['noOfEng'];
    $data['num_of_testers'] = $_POST['noOfTesters'];
    $data['total'] =  $_POST['total'];
    $data['fp'] = $_POST['functionPoint'];
    $data['project_level'] = $_POST['projectType'];
    $data['effort'] = $_POST['effort'];
    $data['time'] = $_POST['time'];
    $data['cost'] = $_POST['cost'];

    $_SESSION['quotation_name'] = $_POST['nameOfProject'];
    $_SESSION['email'] = $_SESSION['logged_in_user']['email'];
    $_SESSION['date'] = $date;
    $_SESSION['num_of_inputs'] = $_POST['noOfInputs'];
    $_SESSION['num_of_outputs'] = $_POST['noOfOutputs'];
    $_SESSION['num_of_files'] = $_POST['noOfFiles'];
    $_SESSION['num_of_external'] = $_POST['noOfExternal'];
    $_SESSION['num_of_engineers'] = $_POST['noOfEng'];
    $_SESSION['num_of_testers'] = $_POST['noOfTesters'];
    $_SESSION['total'] =  $_POST['total'];
    $_SESSION['fp'] = $_POST['functionPoint'];
    $_SESSION['project_level'] = $_POST['projectType'];
    $_SESSION['effort'] = $_POST['effort'];
    $_SESSION['time'] = $_POST['time'];
    $_SESSION['cost'] = $_POST['cost'];
    
    $insRes = Quotation::create($connection,$data);
  
    redirect('./MakePdf.php');
}  