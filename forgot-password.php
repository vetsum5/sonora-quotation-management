<?php
require_once("./app/init.php");
$error = false;
if(Auth::user()) {
    echo("<h3>You are already logged in!</h3>");
    redirect("quotationSys.php", 3);
    die();
}
if(isset($_POST['reset'])) {
    $rules = [
        'email' => [
            'required' => true,
            'maxLength' => 255,
            'email' => true
        ]
    ];

    $validator->check($_POST, $rules);
    if(!$validator->fails()) {
        $email = $_POST['email'];
        $user = User::findByEmail($connection, $email);
        if($user) {
            $token = Token::createForgotPasswordToken($connection, $user['id']);

            if($token) {
                $mail = Mail::getMailer($config->FROM_MAIL_ADDRESS, $config->APP_NAME);
                $mail->addAddress($user['email']);
                $mail->Subject = "Password Recovery";
                $mail->Body = <<<MAIL_BODY
                    <h1>$config->APP_NAME</h1>
                    <p>Use the below link to reset your password!</p>
                    <p><a href='{$config->APP_URL}/reset-password.php?t={$token->token}'>Click Here</a></p>
                MAIL_BODY;
                if($mail->send()) {
                    ed("<h4>Please check your inbox for the reset steps!<h4>");
                } else {
                    ed("Issue with sending mail!");
                }
            } else {
                ed("Error while creating reset link, please try again after sometime!");
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link rel="shortcut icon" href="./Images/logo.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="home-style.css" />
    <link rel="stylesheet" href="forgot-password.css" />
    
    <title>Forgot Password</title>
</head>
<body>
    <div class="container d-flex col-4 align-items-center justify-content-center card pt-5 pb-5 ">
        <form class="card-body d-flex w-75 align-items-center justify-content-center flex-column" action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
            <h3>Forgot Password ?</h3>
        <div class="lock-container w-100 justify-content-center mt-5 mb-2">
            <div class="lock">
                <div class="keyhole"></div>
            </div>
        </div>
        <div class="mb-3 w-75">
            <label for="emailinp" class="form-label">Email address</label>
            <input type="email" name="email" class="form-control" id="emailinp" placeholder="Enter Email address" value="<?= old($_POST, 'email'); ?>">
          </div>
            <!-- <div class="mb-3 w-75">
                <label for="exampleFormControlInput1" class="form-label">Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" id="pwd" placeholder="Enter Password">
                    <button class="btn btn-outline-secondary" type="button" id="showhidepass"><i class="bi bi-eye"></i></button>
                </div>
            </div>
          <div class="d-flex w-75 mt-3 justify-content-center align-items-center">
            <a href="forgot-password.html" style="font-size: small;">Forgot password?</a>
        </div>
        <div class="d-flex w-75 mt-3 justify-content-center align-items-center" style="font-size: small;">
            Don't have an account?&nbsp;<a href="signup.html" style="font-size: small;">Register</a>
        </div> -->
        <div class="d-flex w-75 mt-3 justify-content-center align-items-center">
            <button type="submit" name="reset" id="linkbut" class="btn btn-primary rounded-pill button w-75 ps-4 pe-4">Send Reset Link</button>
        </div>
        </form>
    </div>
    <script src="js/main.js"></script>
</body>
</html>