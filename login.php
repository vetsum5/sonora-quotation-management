<?php
require_once("./app/init.php");

$error = false;

if(isset($_POST['login'])) {
    $rules = [
        'email' => [
            'required' => true,
            'maxLength' => 255,
            'email' => true
        ],
        'password' => [
            'required' => true,
            'minLength' => 8,
            'maxLength' => 255
        ]
    ];
    
    $validator->check($_POST, $rules);
    if(!$validator->fails()) {
        if(Auth::signin($connection, $_POST['email'], $_POST['password'])) {
            if(isset($_POST['remember-me'])) {
                $userToken = Token::createRememberMeToken($connection, Auth::user()['id']);
                setcookie('remember_me', $userToken->token, strtotime(date('Y-m-d H:i:s').Token::EXPIRY_TIME[Token::TYPES['REMEMBER_ME']]));
                // setcookie('remember_me', $userToken->token, strtotime(date('Y-m-d H:i:s')."+5 minutes"));
            }
            $error = false;
            redirect("quotationSys.php");
        } else {
            $error = true;
        }
    }
}

if(accessChatPage($connection)) {
    redirect("quotationSys.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log In</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.7.1.js"
        integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/home-style.css"/>
    <link rel="stylesheet" href="css/loginstyle.css"/>
    <script src="js/login.js"></script>
</head>
<body class="d-flex gap-5 flex-column align-items-center justify-content-center">
   <!-- <div class="container-fluid">
        <button type="button" id="gobackbut" class="btn btn-primary rounded-pill button ps-4 pe-4">Go Back</button>
   </div>  -->
    <!-- <div class="container">
        <div id="head" class="header container-fluid">
            <div class="row pt-3 justify-content-between">
                <div class="col-2 pt-1 pb-2 justify-content-center d-flex align-items-center">
                    <a id="" href="https://www.sonorasoftware.com/#services" class="nav-item">
                        <img src="./Images/Sonora_logo.png" width="100%" alt="" srcset="" />
                    </a>
                </div>
                <div class="col-2 mb-1 d-flex justify-content-center navbut align-items-center" id="navbut-wrapper-1"
                    data-nav-id="1">
                    <a id=""
                        class="button nav-item btn btn-primary d-flex align-items-center justify-content-center text-decoration-none" href="./index.html">
                        Back to Home
                    </a>
                </div>
            </div>
        </div>

    </div> -->

    <div class="container d-flex col-3 align-items-center justify-content-center card pt-5 pb-5 ">
        <form action="<?= $_SERVER['PHP_SELF'];?>" method="POST" class="card-body d-flex align-items-center justify-content-center flex-column" action="">
            <h3>Login</h3>
            <?php 
            if($error):
            ?>
            <div class="input-container d-flex flex-column">
                <span class="error-message" id="loginError">Username and/or Password Incorrect</span>
            </div>
            <?php
            endif;
            ?>  
            <div class="mb-3 w-90">
                <label for="emailinp" class="form-label">Email address</label>
                <input type="email" class="form-control" id="emailinp" name="email" placeholder="Enter Email address" value="<?= old($_POST, 'email'); ?>">
                <span class="error-message" id="emailError">
                    <?= $validator->errors()->has('email') ? $validator->errors()->first('email') : '' ;?>
                </span>
            </div>
            <div class="mb-3 w-90">
                <label for="pwd" class="form-label">Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" name="password" id="pwd" placeholder="Enter Password">
                    <button class="btn btn-outline-secondary" type="button" id="showhidepass"><i class="bi bi-eye"></i></button>
                </div>
                <span class="error-message" id="passwordError">
                    <?= $validator->errors()->has('password') ? $validator->errors()->first('password') : '' ;?>
                </span>
            </div>
            <div class="mb-3 w-90">
                <input type="checkbox" name="remember-me" id="rememberinp">
                <label for="rememberinp" class="form-label">Remember Me</label>
            </div>
            <div class="d-flex w-90 mt-3 justify-content-center align-items-center">
                <button id="loginbut" name="login" class="btn btn-primary rounded-pill button w-50 ps-4 pe-4">Login</button>
            </div>
            <div class="d-flex w-90 mt-3 justify-content-center align-items-center">
                <a href="forgot-password.php" style="font-size: small;">Forgot password?</a>
            </div>
            <div class="d-flex w-90 mt-3 justify-content-center align-items-center" style="font-size: small;">
                Don't have an account?&nbsp;<a href="signup.php" style="font-size: small;">Register</a>
            </div>
        </form>
    </div>

</body>
</html>