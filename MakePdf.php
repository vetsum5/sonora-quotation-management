<?php
    
    require_once("./app/init.php");
require('./fpdf/fpdf.php');
    $pdf = new FPDF('P', 'mm', 'A4');
    $tc = "1. Please pay within 15 days from the date of invoice, overdue interest @ 14% will be charged on delayed payments.\n2. Please quote invoice number when remitting funds.";
    $addnotes = "Please note that any changes or amendments to the quotation must be communicated in writing and agreed upon by both parties. Any additional services requested beyond the scope of this quotation may incur extra charges, subject to negotiation. Thank you for considering our services, and we look forward to the possibility of working with you.";
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);
    $pdf->Image("./Images/Sonora_logo.png",null,null,0,0);
    $pdf->Ln(12);
    $pdf->MultiCell(70,5,"Quotation for\n",0,'L',false);
    $pdf->Ln(1);
    $pdf->SetFont('Arial','',10);
    $pdf->MultiCell(70,5,$_SESSION['quotation_name'] . "\nby " . $_SESSION['email'] ,0,'L',false);
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(25,5,"Invoice date.: ",0,0,'L');
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(70,5,$_SESSION['date'],0,1,'L');
    $pdf->Ln(5);

    // Table Start
    $pdf->SetFont('Arial','B',10);
    $pdf->SetFillColor(144,235,220);
    $pdf->Cell(130,8,"Domain",1,0,'C',true);
    $pdf->Cell(60,8,"Values",1,1,'C',true);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(130,8,"Number of user Inputs",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['num_of_inputs'],1,1,'C');
    $pdf->Cell(130,8,"Number of user Outputs",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['num_of_outputs'],1,1,'C');
    $pdf->Cell(130,8,"Number of Files",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['num_of_files'],1,1,'C');
    $pdf->Cell(130,8,"Number of External Interfaces",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['num_of_external'] ,1,1,'C');
    $pdf->Cell(130,8,"Number of Engineers/Developers",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['num_of_engineers'],1,1,'C');
    $pdf->Cell(130,8,"Number of Testers",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['num_of_testers'],1,1,'C');
    $pdf->Cell(130,8,"Count Total",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['total'],1,1,'C');
    $pdf->Cell(130,8,"Function Point (FP)",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['fp'],1,1,'C');
    $pdf->Cell(130,8,"Project Type",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['project_level'],1,1,'C');
    $pdf->Cell(130,8,"Effort",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['effort'],1,1,'C');
    $pdf->Cell(130,8,"Time required",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['time'],1,1,'C');
    $pdf->Cell(130,8,"Cost",1,0,'C');
    $pdf->Cell(60,8,$_SESSION['cost'],1,1,'C',true);
    // Table End

    $pdf->Ln(7);
    $pdf->SetFont('Arial','B',10);
    $pdf->MultiCell(150,5,"Terms and Conditions\n",0,'L',false);
    $pdf->SetFont('Arial','',10);
    $pdf->Ln(1);
    $pdf->MultiCell(0,5,$tc,0,'J',false);
    $pdf->SetFont('Arial','B',10);
    $pdf->Ln(5);
    $pdf->MultiCell(0,5,"Additional Notes\n",0,'L',false);
    $pdf->Ln(1);
    $pdf->SetFont('Arial','',10);
    $pdf->MultiCell(0,5,$addnotes,0,'J',false);
    $pdf->Ln(10);
    $pdf->SetFont('Arial','B',10);
    $pdf->Image("./Images/sign1.png",167,null,30,10);
    $pdf->Image("./Images/sign2.png",177,null,10,7);
    $pdf->Ln(2);
    $pdf->MultiCell(0,5,"Authorized Signature",0,'R',false);
    $pdf->Ln(7);
    $pdf->Cell(0,15,"For any inquires, email us on sonoraquotationmanager@gmail.com or call us on +91 95036 00151",1,0,'C');
    $pdf->Output();
    $pdf->Close();
?>