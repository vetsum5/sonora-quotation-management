<?php

class Environment {
    private static string $environment = 'development';
    public static function inDebugMode(): bool {
        return self::$environment === 'development';
    }
}