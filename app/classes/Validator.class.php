<?php

class Validator {
    protected array $rules = ['required', 'minLength', 'maxLength', 'unique', 'email'];
    protected array $messages = [
        'required' => 'The :field field is required!',
        'minLength' => 'The :field field must be a minimum of :satisfier characters!',
        'maxLength' => 'The :field field must be a maximum of :satisfier characters!',
        'email' => 'That is not a valid email address!',
        'unique' => 'That :field is already taken!'
    ];

    private QueryBuilder $queryBuilder;
    private ErrorHandler $errorHandler;

    public function __construct(DatabaseConnection $connection, bool $debug) {
        $this->queryBuilder = new QueryBuilder($connection);
        $this->errorHandler = new ErrorHandler();
    }

    public function errors(): ErrorHandler {
        return $this->errorHandler;
    }

    /**
     * Checks the given data against the provided rules
     * 
     * @param array $data The data given to be validated
     * @param array $array The validation rules
     * @return bool Returns true if there are no error, false otherwise
     */
    public function check(array $data, array $rules): bool {
        foreach ($data as $item => $valueToBeValidated) {
            if(isset($rules[$item])) {
                $this->validate($item, $valueToBeValidated, $rules[$item]);
            }
        }
        return !$this->errorHandler->hasErrors();
    }
    public function fails(): bool {
        return $this->errorHandler->hasErrors();
    }

    protected function validate(string $field, mixed $value, array $rules): void {
        foreach($rules as $rule=>$satisfier) {
            if(in_array($rule, $this->rules) && method_exists($this, $rule)) {
                if(!call_user_func_array([$this, $rule], [$field, $value, $satisfier])) {
                    $message = str_replace([':field', ':satisfier'], [$field, $satisfier], $this->messages[$rule]) ?? 'Invalid Validation Rule!';
                    $this->errorHandler->addError($field, $message);
                }
            }
        }
    }

    protected function required(string $field, mixed $value, mixed $satisfier): bool {
        return !empty(trim($value));
    }
    protected function minLength(string $field, mixed $value, mixed $satisfier): bool {
        return mb_strlen(trim($value)) >= $satisfier;
    }
    protected function maxLength(string $field, mixed $value, mixed $satisfier): bool {
        return mb_strlen(trim($value)) <= $satisfier;
    }
    protected function email(string $field, mixed $value, mixed $satisfier): bool {
        return (bool) filter_var($value, FILTER_VALIDATE_EMAIL);
    }
    protected function unique(string $field, mixed $value, mixed $satisfier): bool {
        $tablename = explode(".", $satisfier)[0];
        $columnname = explode(".", $satisfier)[1];

        return !$this->queryBuilder->table($tablename)
                                   ->exists([$columnname=>$value]);
    }
}