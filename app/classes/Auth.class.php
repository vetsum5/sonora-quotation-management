<?php
define('USER_KEY', 'logged_in_user');

class Auth {
    public static function signin(DatabaseConnection $connection, string $email, string $password): bool {
        $user = User::findByEmail($connection, $email);
        if($user) {
            $dbPassword = $user['password'];
            if(Hash::verify($password, $dbPassword)) {
                self::setLoggedInUser($user);
                return true;
            }
        }
        return false;
    }

    public static function setLoggedInUser(array $dbUser): void {
        $_SESSION[USER_KEY] = $dbUser;
    }

    public static function user(): mixed {
        if(isset($_SESSION[USER_KEY])) {
            return $_SESSION[USER_KEY];
        }
        return null;
    }

    public static function logout(DatabaseConnection $connection): void {
        Token::deleteRememberMeToken($connection, $_SESSION[USER_KEY]['id']);
        unset($_SESSION[USER_KEY]);
        setcookie('remember_me', "", time()-3600);
    }
}