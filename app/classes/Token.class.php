<?php

class Token {
    private QueryBuilder $queryBuilder;
    
    protected static $table = "tokens";
    public const TYPES = [
        'REMEMBER_ME' => 0,
        'FORGOT_PASSWORD' => 1
    ];
    public const EXPIRY_TIME = [
        self::TYPES['REMEMBER_ME'] => '+30 minutes',
        self::TYPES['FORGOT_PASSWORD'] => '+10 minutes'
    ];

    public function __construct(DatabaseConnection $connection, bool $debug) {
        $this->queryBuilder = new QueryBuilder($connection);
    }

    public static function migrate(DatabaseConnection $connection, bool $debug) {
        try {
            $queryBuilder = new QueryBuilder($connection);
            $table = self::$table;
            $ddl = <<<DDL
            CREATE TABLE IF NOT EXISTS {$table} (
                                            id INT(11) PRIMARY KEY AUTO_INCREMENT,
                                            user_id int(11),
                                            token VARCHAR(255) UNIQUE,
                                            expires_at DATETIME NOT NULL,
                                            type TINYINT
                                        );
            DDL;
            return $queryBuilder->executeDDL($ddl);
        } catch(Exception $e) {
            dd($debug ? $e->getMessage() : "Migration Failed");
        }
    }

    public static function createForgotPasswordToken(DatabaseConnection $connection, int $userId) {
        return self::create($connection, $userId, self::TYPES['FORGOT_PASSWORD']);
    }
    public static function createRememberMeToken(DatabaseConnection $connection, int $userId) {
        return self::create($connection, $userId, self::TYPES['REMEMBER_ME']);
    }
    
    public static function getValidExistingToken(DatabaseConnection $connection, int $userId, int $type) {
        $queryBuilder = new QueryBuilder($connection);
        $token = $queryBuilder->table(self::$table)
                              ->where('user_id', '=', $userId)
                              ->where('type', '=', $type)
                              ->where('expires_at', '>', date('Y-m-d H:i:s'))
                              ->first();
        return $token;
    }
    
    private static function create(DatabaseConnection $connection, int $userId, int $type): mixed {
        $token = self::getValidExistingToken($connection, $userId, $type);
        if($token) {
            return (object)$token;
        }
        $data['user_id'] = $userId;
        $data['token'] = Hash::generateToken($userId);
        $data['type'] = $type;
        
        $expiryTime = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . self::EXPIRY_TIME[$type]));
        $data['expires_at'] = $expiryTime;
        
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->insert($data) ? (object)$data : [];
    }
    
    public static function isValidRememberMeToken(DatabaseConnection $connection, string $token): mixed {
        return self::isValid($connection, $token, self::TYPES['REMEMBER_ME']);
    }
    public static function isValidForgotPasswordToken(DatabaseConnection $connection, string $token): mixed {
        return self::isValid($connection, $token, self::TYPES['FORGOT_PASSWORD']);
    }
    
    private static function isValid(DatabaseConnection $connection, string $token, int $type): mixed {
        $queryBuilder = new QueryBuilder($connection);
        $currentDateTime = date('Y-m-d H:i:s');
        return $queryBuilder->table(self::$table)
                            ->where('token', '=', $token)
                            ->where('type', '=', $type)
                            ->where('expires_at', '>', $currentDateTime)
                            ->first();
    }

    public static function deleteForgotPasswordToken(DatabaseConnection $connection, int $userId): bool {
        return self::deleteToken($connection, $userId, self::TYPES['FORGOT_PASSWORD']);
    }
    public static function deleteRememberMeToken(DatabaseConnection $connection, int $userId): bool {
        return self::deleteToken($connection, $userId, self::TYPES['REMEMBER_ME']);
    }

    private static function deleteToken(DatabaseConnection $connection, int $userId, int $type): bool {
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('user_id', '=', $userId)
                            ->where('type', '=', $type)
                            ->delete();
    }
}