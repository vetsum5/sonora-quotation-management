<?php

class DatabaseCreate {
    private QueryBuilder $queryBuilder;
    private string $dbName;
    public function __construct(DatabaseConnection $conn, string $dbName) {
        // dd($config->DB_NAME);
        $this->queryBuilder = new QueryBuilder($conn);
        $this->dbName = $dbName;
        
    }

    public function createDbAndUse() {
        $query = "CREATE DATABASE IF NOT EXISTS $this->dbName;";
        if($this->queryBuilder->executeDDL($query)) {
            // dd("Here");
            $this->queryBuilder->executeDDL("USE $this->dbName;");
        }
    }
}