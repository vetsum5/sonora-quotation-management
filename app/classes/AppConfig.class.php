<?php

class AppConfig {
    private static $instance = null;
    
    public string $APP_NAME;
    public string $APP_URL;
    public string $DB_DRIVER;
    public string $DB_NAME;
    public string $DB_HOST;
    public string $DB_PORT;
    public string $DB_USER;
    public string $DB_PASS;
    public string $FROM_MAIL_ADDRESS;
    public bool $APP_DEBUG = false;

    private function __construct() {}

    public static function getInstance(string $rootPath = "./config.ini"): self {
        if(self::$instance === null) {
            self::$instance = new self();
        }
        self::loadEnv($rootPath);
        return self::$instance;
    }

    private static function loadEnv(string $rootPath): void {
        try {
            $config = parse_ini_file($rootPath);
            if(isset($config['app_debug'])) {
                self::$instance->APP_DEBUG = $config['app_debug'] !== "false";
                self::$instance->APP_NAME = $config['app_name'];
                self::$instance->APP_URL = $config['app_url'];
                self::$instance->DB_DRIVER = $config['driver_name'];
                self::$instance->DB_HOST = $config['host'];
                self::$instance->DB_NAME = $config['database'];
                self::$instance->DB_PORT = $config['port'];
                self::$instance->DB_USER = $config['username'];
                self::$instance->DB_PASS = $config['password'];
                self::$instance->FROM_MAIL_ADDRESS = $config['from_mail_address'];
            }
        } catch (Exception $e) {
            dd(self::$instance->APP_DEBUG ? $e->getMessage() : "Unable to load config properly!");
        }
    }
}