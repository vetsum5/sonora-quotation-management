<?php
class DatabaseConnection {
    private ?PDO $pdo = null;

    public function __construct(private AppConfig $config)
    {
        $this->connect();
    }
    
    private function connect(): void {
        try {
            $dsn = "{$this->config->DB_DRIVER}:host={$this->config->DB_HOST};port={$this->config->DB_PORT}";
            $this->pdo = new PDO($dsn, $this->config->DB_USER, $this->config->DB_PASS);
            if($this->config->APP_DEBUG) {
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
        } catch (PDOException $e) {
            die($this->config->APP_DEBUG ? $e->getMessage() : "Some error while connecting to database!");
        }
    }

    public function getPdo(): ?PDO {
        return $this->pdo;
    }

}