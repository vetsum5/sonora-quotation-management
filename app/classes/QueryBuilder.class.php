<?php

class QueryBuilder {
    private ?PDO $pdo = null;
    private ?PDOStatement $stmt = null;
    private string $table;

    protected array $whereConditions = [];
    protected array $bindings = [];
    protected int $offset = -1;
    protected int $numberOfRows = -1;

    public function __construct(DatabaseConnection $connection) {
        $this->pdo = $connection->getPdo();
    }
    
    /**
     * Executes any Raw DDL query passed to the function
     * @param $sql: DDL statement to be fired
     * @return bool Returns true on success otherwise false
     */
    public function executeDDL(string $sql): bool {
        return $this->pdo->exec($sql) !== false;
    }

    public function table(string $table): self {
        $this->table = $table;
        return $this;
    }

    public function insert(array $data): bool {
        $keys = array_keys($data);
        $fields = "`" . implode("`, `", $keys) . "`";
        $placeholders = ":" . implode(", :", $keys);
        $sql = "INSERT INTO {$this->table} ($fields) VALUES ($placeholders);";
        $this->stmt = $this->pdo->prepare($sql);

        $data = $this->stmt->execute($data);
        $this->reset();
        return $data;
    }

    public function where(string $field, string $operator, string $value): self {
        $this->whereConditions[] = "`$field` $operator :where$field";
        $this->bindings["where$field"] = $value;

        return $this;
    }

    public function delete(): bool {
        $sql = "DELETE FROM `$this->table`" . $this->bindWhereClause() . ";";
        $this->stmt = $this->pdo->prepare($sql);
        $data = $this->stmt->execute($this->bindings);
        $this->reset();
        return $data;
    }
    
    public function count(): int {
        $sql = "SELECT COUNT(*) AS count FROM {$this->table}" . $this->bindWhereClause() . ";";
        $this->stmt = $this->pdo->prepare($sql);
        $this->stmt->execute($this->bindings);
        $data = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->reset();
        return $data[0]['count'];
    }

    public function limit(int $offset, int $numberOfRows): self {
        $this->offset = $offset;
        $this->numberOfRows = $numberOfRows;
        return $this;
    }
    
    public function get(array $columns = []): array {
        if(empty($columns)) {
            $selectColumns = "*";
        } else {
            $selectColumns = implode(", ", $columns);
        }
        $sql = "SELECT $selectColumns FROM {$this->table}" . $this->bindWhereClause();
        
        if($this->numberOfRows !== -1) {
            $sql .= " LIMIT {$this->offset}, {$this->numberOfRows};";
        } else {
            $sql .= ";";
        }

        $this->stmt = $this->pdo->prepare($sql);
        $this->stmt->execute($this->bindings);
        $data = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->reset();
        return $data;
    }

    public function update(array $data): bool {
        $setClause = implode(", ", array_map(function ($field) {
            return "`$field` = :update$field";
        }, array_keys($data)));

        $sql = "UPDATE {$this->table} SET $setClause" . $this->bindWhereClause() . ";";
        $this->stmt = $this->pdo->prepare($sql);
        $this->bindValues($data);
        $data = $this->stmt->execute($this->bindings);
        $this->reset();
        return $data;
    }

    public function exists(array $data): bool {
        foreach($data as $key=>$value) {
            $this->where($key, '=', $value);
        }
        return $this->count();
    }

    public function first(): array|bool {
        $this->numberOfRows = 1;
        $this->offset = 0;
        $data = $this->get();
        return !empty($data) ? $data[0] : false;
    }

    private function reset() {
        $this->whereConditions = [];
        $this->bindings = [];
        $this->offset = -1;
        $this->numberOfRows = -1;
        $this->stmt = null;
    }
    
    private function bindValues(array $data): void {
        foreach ($data as $key => $value) {
            $this->bindings["update$key"] = $value;
        }
    }

    private function bindWhereClause(): string {
        if(empty($this->whereConditions)) {
            return '';
        }

        return " WHERE " . implode(" AND ", $this->whereConditions);
    }
}