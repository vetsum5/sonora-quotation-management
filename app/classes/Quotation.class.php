<?php

class Quotation {
    private QueryBuilder $queryBuilder;
    
    protected static $table = "quotations";

    public function __construct(DatabaseConnection $connection, bool $debug) {
        $this->queryBuilder = new QueryBuilder($connection);
    }

    public static function migrate(DatabaseConnection $connection, bool $debug) {
        try {
            $queryBuilder = new QueryBuilder($connection);
            $table = self::$table;
            $ddl = <<<DDL
            CREATE TABLE IF NOT EXISTS {$table} (
                                            id INT(11) PRIMARY KEY AUTO_INCREMENT,
                                            email VARCHAR(255),
                                            quotation_name VARCHAR(255),
                                            date VARCHAR(255),
                                            num_of_inputs VARCHAR(255),
                                            num_of_outputs VARCHAR(255),
                                            num_of_files VARCHAR(255),
                                            num_of_external VARCHAR(255),
                                            num_of_engineers VARCHAR(255),
                                            num_of_testers VARCHAR(255),
                                            total VARCHAR(255),
                                            fp VARCHAR(255),
                                            project_level VARCHAR(255),
                                            effort VARCHAR(255),
                                            time VARCHAR(255),
                                            cost VARCHAR(255)
                                        );
            DDL;
            return $queryBuilder->executeDDL($ddl);
        } catch(Exception $e) {
            dd($debug ? $e->getMessage() : "Migration Failed");
        }
    }

    public static function create(DatabaseConnection $connection, array $data): mixed {
        
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->insert($data);
    }

    // public static function updatePassword(DatabaseConnection $connection, int $userId, array $data): mixed {
    //     if(!isset($data['password'])) {
    //         return false;
    //     }
    //     $data['password'] = Hash::make($data['password']);
    //     $queryBuilder = new QueryBuilder($connection);
    //     return $queryBuilder->table(self::$table)
    //                         ->where('id', '=', $userId)
    //                         ->update($data);
    // }

    // public static function update(DatabaseConnection $connection, array $data): mixed {
    //     if(!isset($data['password'])) {
    //         return false;
    //     }
    //     $data['password'] = Hash::make($data['password']);
    //     $queryBuilder = new QueryBuilder($connection);
    //     return $queryBuilder->table(self::$table)
    //                         ->update($data);
    // }

    public static function findByEmail(DatabaseConnection $connection, string $email): array | bool {
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('email', '=', $email)
                            ->get();
    }

    // public static function findAnswers(DatabaseConnection $connection, string $id): array | bool {
    //     $queryBuilder = new QueryBuilder($connection);
    //     $cols = ['q1_response','q2_response','q3_response','q4_response','q5_response','q6_response','q7_response','q8_response','q9_response','q10_response'];
    //     return $queryBuilder->table(self::$table)
    //                         ->where('id', '=', $id)
    //                         ->get($cols);
    // }

    public static function findByName(DatabaseConnection $connection, string $quotname): array | bool {
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('quotation_name', '=', $quotname)
                            ->first();
    }
    
    public static function find(DatabaseConnection $connection, int $id): array | bool {
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('id', '=', $id)
                            ->first();
    }

    public static function deleteQuotation(DatabaseConnection $connection, int $id){
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('id', '=', $id)
                            ->delete();
    }
}