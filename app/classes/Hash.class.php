<?php

class Hash {
    static public function make(string $plainText): string {
        return password_hash($plainText, PASSWORD_BCRYPT);
    }

    static public function verify(string $password, string $hashedPassword): bool {
        return password_verify($password, $hashedPassword);
    }

    static public function generateToken(int $userId = 0) {
        return hash('sha256', "$userId" . random_bytes(10) . getCurrentTimeInMillis() . strrev($userId) . random_bytes(10));
    }
}