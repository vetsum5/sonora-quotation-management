<?php

class User {
    private QueryBuilder $queryBuilder;
    
    protected static $table = "users";

    public function __construct(DatabaseConnection $connection, bool $debug) {
        $this->queryBuilder = new QueryBuilder($connection);
    }

    public static function migrate(DatabaseConnection $connection, bool $debug, string $dbName) {
        try {
            $createDb = new DatabaseCreate($connection, $dbName);
            $createDb->createDbAndUse();
            // dd($connection->getPdo());
            $queryBuilder = new QueryBuilder($connection);
            $table = self::$table;
            $ddl = <<<DDL
            CREATE TABLE IF NOT EXISTS {$table} (
                                            id INT(11) PRIMARY KEY AUTO_INCREMENT,
                                            firstname VARCHAR(255),
                                            lastname VARCHAR(255),
                                            email VARCHAR(255) UNIQUE NOT NULL,
                                            password VARCHAR(255),
                                            role TINYINT
                                        );
            DDL;
            return $queryBuilder->executeDDL($ddl);
        } catch(Exception $e) {
            dd($debug ? $e->getMessage() : "Migration Failed");
        }
    }

    public static function create(DatabaseConnection $connection, array $data): mixed {
        if(!isset($data['password'])) {
            return false;
        }
        $data['password'] = Hash::make($data['password']);
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->insert($data);
    }

    public static function updatePassword(DatabaseConnection $connection, int $userId, array $data): mixed {
        if(!isset($data['password'])) {
            return false;
        }
        $data['password'] = Hash::make($data['password']);
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('id', '=', $userId)
                            ->update($data);
    }

    public static function update(DatabaseConnection $connection, array $data): mixed {
        if(!isset($data['password'])) {
            return false;
        }
        $data['password'] = Hash::make($data['password']);
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->update($data);
    }

    public static function findByEmail(DatabaseConnection $connection, string $email): array | bool {
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('email', '=', $email)
                            ->first();
    }
    
    public static function find(DatabaseConnection $connection, int $id): array | bool {
        $queryBuilder = new QueryBuilder($connection);
        return $queryBuilder->table(self::$table)
                            ->where('id', '=', $id)
                            ->first();
    }
}