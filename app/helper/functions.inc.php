<?php

function dd($data) {
    die(var_dump($data));
}

function ed($data) {
    echo($data);
    die();
}

function redirect(string $url, int $refresh=0) {
    if($refresh > 0){
        header("Refresh: $refresh;url=$url");
    } else {
        header("Location: $url");
    }
}

function old($collection, $key, $default="") {
    return isset($collection[$key]) ? $collection[$key] : $default;
}

function getCurrentTimeInMillis() {
    return round(microtime(true) * 1000);
}

function accessChatPage(DatabaseConnection $connection): bool {
    if(isset($_COOKIE['remember_me'])) {
        $rememberMeToken = $_COOKIE['remember_me'];
        $tokenData = Token::isValidRememberMeToken($connection, $rememberMeToken);

        if($tokenData) {
            $userId = $tokenData['user_id'];
            $user = User::find($connection, $userId);
            Auth::setLoggedInUser($user);
            return true;
        }
    }
    return false;
}