<?php
session_start();
$app = __DIR__;
require_once("{$app}/helper/functions.inc.php");
require_once("{$app}/../vendor/autoload.php");
require_once("{$app}/classes/Mail.class.php");
require_once("{$app}/classes/Hash.class.php");
require_once("{$app}/classes/AppConfig.class.php");
require_once("{$app}/classes/ErrorHandler.class.php");
require_once("{$app}/classes/DatabaseConnection.class.php");
require_once("{$app}/classes/QueryBuilder.class.php");
require_once("{$app}/classes/Validator.class.php");
require_once("{$app}/classes/User.class.php");
require_once("{$app}/classes/DatabaseCreate.class.php");
require_once("{$app}/classes/Token.class.php");
require_once("{$app}/classes/Quotation.class.php");
require_once("{$app}/classes/Auth.class.php");

$config = AppConfig::getInstance();
$connection = new DatabaseConnection($config);
$validator = new Validator($connection, $config->APP_DEBUG);
User::migrate($connection, $config->APP_DEBUG, $config->DB_NAME);
Token::migrate($connection, $config->APP_DEBUG);
Quotation::migrate($connection,$config->APP_DEBUG);